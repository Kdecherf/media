# Copyright 2012-2015 Johannes Nixdorf <mixi@exherbo.org>
# Based in part upon 'mplayer2.exlib', which is
#   Copyright 2011 Elias Pipping <pipping@exherbo.org>
#   Copyright 2011 Alex Elsayed <eternaleye@gmail.com>
# Distributed under the terms of the GNU General Public License v2

# last checked revision: f59371de2170141fc28540d03c4e7ecc62844ebf

require freedesktop-desktop github [ user="mpv-player" tag="v${PV}" ] gtk-icon-cache waf \
        lua [ multibuild=false whitelist="5.1 5.2" with_opt=true ]

export_exlib_phases src_prepare src_configure pkg_postrm pkg_postinst

if ever at_least 0.29; then
    WAF_VER=2.0.9
else
    WAF_VER=1.8.12
fi

SUMMARY="Video player based on MPlayer/mplayer2"
HOMEPAGE="https://mpv.io"
DOWNLOADS+=" https://waf.io/pub/release/waf-${WAF_VER}"

UPSTREAM_RELEASE_NOTES="https://github.com/mpv-player/mpv/releases"

LICENCES="GPL-2 GPL-3 LGPL-2.1"
SLOT="0"

# TODO(sardemff7): Drop dvd-menu option as there is no menu support in mpv
MYOPTIONS="
    alsa
    bluray      [[ description = [ Adds support for video blurays ] ]]
    cd          [[ description = [ Adds support for audio CDs ] ]]
    drm         [[ description = [ Adds support for a video output directly to the framebuffer ] ]]
    dvd
    dvd-menu    [[ description = [ Adds dvd menu support through libdvdnav ]
                   requires = [ dvd ] ]]
    icc         [[ description = [ Adds support for using ICC profiles through lcms2 ] ]]
    lua         [[ description = [ Adds lua scripting support and an onscreen controller ] ]]
    opengl
    pulseaudio
    sndio       [[ description = [ Adds support for sound output through sndio (OpenBSD sound API, also ported to Linux) ] ]]
    va          [[ description = [ Adds support for decoding and presenting video using the Video Acceleration API ]
                   requires = [ providers: ffmpeg ] ]]
    vdpau       [[ description = [ Adds support for presenting and decoding video using the VDPAU API (-vo=vdpau and -hwdec=vdpau) ]
                   requires = [ providers: ffmpeg ] ]]
    vulkan      [[ description = [ Support for the Vulkan API ] ]]
    wayland
    X
    zsh-completion [[ description = [ Generate and install a zsh completion file ] ]]
    ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# disabled because i can't figure out waf
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/python:*
        dev-python/docutils
        virtual/pkg-config
        X? ( x11-proto/xorgproto )
        vulkan? ( sys-libs/vulkan-headers )
        zsh-completion? ( dev-lang/perl:*[>=5.8] )
    build+run:
        media-libs/libass[fontconfig][>=0.12.1]
        sys-libs/ncurses
        sys-libs/zlib
        alsa? ( sys-sound/alsa-lib[>=1.0.18] )
        bluray? ( media-libs/libbluray[>=0.3.0] )
        cd? (
            dev-libs/libcdio
            dev-libs/libcdio-paranoia
        )
        drm? ( x11-dri/libdrm )
        dvd? ( media-libs/libdvdread[>=4.1.0] )
        dvd-menu? ( media-libs/libdvdnav[>=4.2.0] )
        icc? ( media-libs/lcms2[>=2.6] )
        opengl? ( x11-dri/mesa[>=9.0.0][X?][wayland?] )
        providers:ffmpeg? ( media/ffmpeg[>=3.3][va?][vdpau?] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:libav? ( media/libav[>=12] )
        pulseaudio? ( media-sound/pulseaudio[>=1.0] )
        sndio? ( sys-sound/sndio )
        va? ( x11-libs/libva[>=1.4.0][X?][wayland?] )
        vdpau? ( x11-libs/libvdpau[>=0.2] )
        vulkan? (
            sys-libs/shaderc
            sys-libs/vulkan-loader
        )
        wayland? (
            sys-libs/wayland[>=1.6.0]
            x11-libs/libxkbcommon[>=0.3.0]
        )
        X? (
            x11-libs/libX11[>=1.0.0]
            x11-libs/libXScrnSaver[>=1.0.0]
            x11-libs/libXext[>=1.0.0]
            x11-libs/libXinerama[>=1.0.0]
            x11-libs/libXrandr[>=1.2.0]
            x11-libs/libXv
        )
    suggestion:
        lua? ( net-misc/youtube-dl[>=2015.02.23.1] [[ description = [ Support to play videos from YouTube and other video sites ] ]] )
"

if ever at_least 0.29; then
    DEPENDENCIES+="
        build+run:
            providers:ffmpeg? ( media/ffmpeg[>=4.0][va?][vdpau?] )
            wayland? ( sys-libs/wayland-protocols )
    "
fi

mpv_src_prepare() {
    edo cp "${FETCHEDDIR}"/waf-${WAF_VER} waf
    chmod +x waf

    default
}

mpv_src_configure() {
    local args=(
        ## paths
        --prefix=/usr/$(exhost --target)
        --confdir=/etc/${PN}
        --libdir=/usr/$(exhost --target)/lib
        --datadir=/usr/share
        --docdir=/usr/share/doc/${PNVR}
        --htmldir=/usr/share/doc/${PNVR}/html
        --mandir=/usr/share/man

        ## doc
        --enable-manpage-build
        --disable-html-build
        --disable-pdf-build

        ## misc
        --enable-iconv
        --enable-jpeg
        --enable-libass
        --enable-libass-osd
        --enable-libmpv-shared
        --disable-apple-remote
        --disable-javascript
        --disable-macos-cocoa-cb
        --disable-macos-touchbar
        --disable-test
        --disable-uchardet
        --disable-vapoursynth
        --disable-vapoursynth-lazy
        $(option lua --lua=${LUA_ABIS/./}fbsd --disable-lua)
        $(option_enable zsh-completion zsh-comp)

        ## sources
        --enable-audio-input
        --enable-dvbin
        --enable-tv
        --enable-tv-v4l2
        --disable-libarchive
        --disable-libsmbclient
        --disable-libv4l2
        $(option_enable bluray libbluray)
        $(option_enable cd cdda)
        $(option_enable dvd dvdread)
        $(option_enable dvd-menu dvdnav)
        $(option_enable providers:ffmpeg libavdevice)

        ## audio
        --disable-rubberband

        ## audio out
        --enable-oss-audio
        --disable-audiounit
        --disable-coreaudio
        --disable-jack
        --disable-openal
        --disable-opensles
        --disable-rsound
        --disable-sdl2
        --disable-sndio
        --disable-wasapi
        $(option_enable alsa)
        $(option_enable pulseaudio pulse)
        $(option_enable sndio)

        ## video
        --disable-caca
        $(option_enable icc lcms2)

        ## video out
        --disable-crossc

        $(option_enable drm)
        $(option_enable drm drmprime)
        $(if option drm; then
            option_enable opengl egl-drm
            option_enable va vaapi-drm
        fi)

        $(option_enable opengl gl)
        $(option_enable opengl plain-gl)

        $(option_enable vulkan shaderc)
        $(option_enable vulkan)

        $(option_enable wayland)
        $(option_enable wayland wayland-protocols)
        $(option_enable wayland wayland-scanner)
        $(if option wayland; then
            option_enable opengl gl-wayland
            option_enable va vaapi-wayland
        fi)

        $(option_enable X x11)
        $(option_enable X xv)
        $(if option X; then
            option_enable opengl gl-x11
            option_enable opengl egl-x11
            option_enable va vaapi-x11
            if option opengl; then
                option_enable va vaapi-glx
                option_enable vdpau vdpau-gl-x11
            fi
        fi)

        ## platform
        --disable-android
        --disable-cocoa
        --disable-d3d11
        --disable-direct3d
        --disable-egl-angle
        --disable-gl-cocoa
        --disable-gl-win32
        --disable-ios-gl
        --disable-mali-fbdev
        --disable-rpi
        --disable-swift
        --disable-uwp
        --disable-videotoolbox-gl

        ## hwaccel
        --disable-cuda-hwaccel
        --disable-d3d-hwaccel
        --disable-d3d9-hwaccel
        --disable-gl-dxinterop-d3d9
        $(option_enable va vaapi)
        $(option_enable vdpau)
    )

    ewaf --jobs=1 configure \
       "${args[@]}"
}

mpv_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

mpv_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

