# Copyright 2009 Richard Brown <rbrown@exherbo.org>
# Copyright 2011, 2012 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]
require alternatives

if ever is_scm; then
    require gnome.org
else
    DOWNLOADS="https://download.gimp.org/pub/${PN}/$(ever range 1-2)/${PNV}.tar.bz2"
fi

export_exlib_phases src_prepare src_configure src_install

SUMMARY="GEGL (Generic Graphics Library) is a graph based image processing framework"
DESCRIPTION="
GEGL provides infrastructure to do demand based cached non destructive image editing
on larger than RAM buffers. Through babl it provides support for a wide range of color
models and pixel storage formats for input and output.
"
HOMEPAGE="http://${PN}.org"

UPSTREAM_RELEASE_NOTES="https://git.gnome.org/browse/${PN}/plain/NEWS"

LICENCES="GPL-3 LGPL-3"
MYOPTIONS="
    exif ffmpeg gobject-introspection jpeg2000 lcms openexr raw sdl svg tiff
    v4l  [[ description = [ V4L2 device support ] ]]
    webp [[ description = [ Support for the WebP image format ] ]]
    ffmpeg? ( ( providers: ffmpeg libav ) [[ number-selected = exactly-one ]] )
    ( x86_cpu_features: mmx sse )
    ( platform: amd64 )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# TODO: --enable-docs has automagic deps upon asciidoc, enscript & ruby
# Also, they always rebuild and only install if the tools above are present.
# See:  https://bugzilla.gnome.org/show_bug.cgi?id=676688
DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.8]
        virtual/pkg-config
    build+run:
        core/json-glib
        dev-libs/glib:2[>=2.44.0]
        media-libs/libpng:=[>=1.6.0]
        x11-libs/cairo[>=1.12.2]
        x11-libs/gdk-pixbuf:2.0[>=2.32.0]
        x11-libs/pango[>=1.38.0]
        exif? ( graphics/exiv2[>=0.25] )
        ffmpeg? (
            providers:ffmpeg? ( media/ffmpeg[>=2.3] )
            providers:libav? ( media/libav[>=9.1] )
        )
        gobject-introspection? ( gnome-desktop/gobject-introspection[>=1.32.0] )
        jpeg2000? ( media-libs/jasper[>=1.900.1] )
        lcms? ( media-libs/lcms2[>=2.8] )
        openexr? ( media-libs/openexr[>=1.6.1] )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        sdl? ( media-libs/SDL:0[>=1.2.0] )
        svg? ( gnome-desktop/librsvg:2[>=2.40.6] )
        raw? ( media-libs/libraw[>=0.15.4] )
        tiff? ( media-libs/tiff[>=4.0.0] )
        v4l? ( media-libs/v4l-utils[>=1.0.1] )
        webp? ( media-libs/libwebp:=[>=0.5.0] )
        !media-libs/gegl:0[<=0.2.0-r7] [[
            description = [ /usr/bin/gegl collides ]
            resolution = upgrade-blocked-before
        ]]
"
# needs babl[gobject-introspection?], which is broken
# See https://bugzilla.gnome.org/show_bug.cgi?id=673422
#    build:
#        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=1.32.0] )
# tests fail softly with introspection
#    test:
#        gobject-introspection? ( dev-lang/python:*[>=2.5]
#                                 gnome-bindings/pygobject:3[>=3.2] )

if ever at_least 0.4.10 ; then
    DEPENDENCIES+="
        build+run:
            media-libs/babl[$(ever is_scm && echo '~scm' || echo '>=0.1.58')]
    "
else
    DEPENDENCIES+="
        build+run:
            media-libs/babl[>=0.1.48]
    "
fi

AT_M4DIR=( m4 )

gegl_src_prepare() {
    # Disable failing test, last checked: 0.4.0
    # /bin/sh: line 1: ../../examples/gegl-video: No such file or directory
    edo sed -e '/ff-load-save/d' -i tests/Makefile.am

    autotools_src_prepare
}

gegl_src_configure() {
    local myconf=()

    myconf+=(
        --prefix=/usr
        --exec_prefix=/usr/$(exhost --target)
        --includedir=/usr/$(exhost --target)/include
        # 'gcut' tool name conflicts with cut from coreutils
        # https://bugzilla.gnome.org/show_bug.cgi?id=786838
        --program-transform-name='s/^gcut$/gegl-gcut/'
        --disable-docs
        --with-cairo
        --with-gdk-pixbuf
        --with-pango
        --with-pangocairo
        # mrg and gexiv2 needed for gui
        --without-gexiv2
        --without-graphviz
        --without-lensfun
        --without-libspiro
        --without-lua
        --without-mrg # unpackaged
        --without-umfpack
        --without-vala # currently broken, upstream has a fix
        $(option_enable gobject-introspection introspection)
        $(option_with exif exiv2)
        $(option_with ffmpeg libavformat)
        $(option_with jpeg2000 jasper)
        $(option_with lcms)
        $(option_with openexr)
        $(option_with raw libraw)
        $(option_with sdl)
        $(option_with svg librsvg)
        $(option_with tiff libtiff)
        $(option_with v4l libv4l)
        $(option_with v4l libv4l2)
        $(option_with webp)
    )

    # If I don't do this, they are forced off
    if option platform:amd64 ; then
        myconf+=(
            --enable-mmx
            --enable-sse
        )
    else
        myconf+=(
            $(option_enable x86_cpu_features:mmx)
            $(option_enable x86_cpu_features:sse)
        )
    fi

    econf "${myconf[@]}"
}

gegl_src_install() {
    default

    local alternatives=(
        /usr/$(exhost --target)/bin/${PN} ${PN}-${SLOT}
        /usr/$(exhost --target)/bin/${PN}-gcut ${PN}-gcut-${SLOT}
        /usr/$(exhost --target)/bin/${PN}-imgcmp ${PN}-imgcmp-${SLOT}
    )

    alternatives_for _${PN} ${PN}-${SLOT} ${SLOT} "${alternatives[@]}"
}

