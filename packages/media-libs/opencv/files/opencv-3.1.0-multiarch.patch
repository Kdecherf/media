Upstream: See https://github.com/Itseez/opencv/pull/5940

From dd4f2352eb3e26737b03cb170eebafea17a352ac Mon Sep 17 00:00:00 2001
From: Niels Ole Salscheider <niels_ole@salscheider-online.de>
Date: Sat, 9 Jan 2016 15:09:56 +0100
Subject: [PATCH] Always install CMake files to lib/cmake/OpenCV

"share" is only for architecture-independent files.

This change is important for multi-arch distributions like Exherbo that
allow to install a package for several architectures in parallel.
Here, architecture-independent files are installed to /usr/share while
all architecture-dependent files are installed to
/usr/${host}/{bin,include,lib}. LIB_SUFFIX is not set here.
---
 CMakeLists.txt                        | 7 +------
 cmake/templates/OpenCVConfig.cmake.in | 6 +-----
 2 files changed, 2 insertions(+), 11 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index bf9d85f..7e367ad 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -389,12 +389,7 @@ else()
   endif()
   set(OPENCV_INCLUDE_INSTALL_PATH "include")
 
-  math(EXPR SIZEOF_VOID_P_BITS "8 * ${CMAKE_SIZEOF_VOID_P}")
-  if(LIB_SUFFIX AND NOT SIZEOF_VOID_P_BITS EQUAL LIB_SUFFIX)
-    set(OPENCV_CONFIG_INSTALL_PATH lib${LIB_SUFFIX}/cmake/opencv)
-  else()
-    set(OPENCV_CONFIG_INSTALL_PATH share/OpenCV)
-  endif()
+  set(OPENCV_CONFIG_INSTALL_PATH lib${LIB_SUFFIX}/cmake/opencv)
 endif()
 
 set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/${OPENCV_LIB_INSTALL_PATH}")
diff --git a/cmake/templates/OpenCVConfig.cmake.in b/cmake/templates/OpenCVConfig.cmake.in
index 80ffbaf..0abd84e 100644
--- a/cmake/templates/OpenCVConfig.cmake.in
+++ b/cmake/templates/OpenCVConfig.cmake.in
@@ -113,11 +113,7 @@ set(OpenCV_USE_MANGLED_PATHS @OpenCV_USE_MANGLED_PATHS_CONFIGCMAKE@)
 get_filename_component(OpenCV_CONFIG_PATH "${CMAKE_CURRENT_LIST_FILE}" PATH CACHE)
 
 if(NOT WIN32 OR ANDROID)
-  if(ANDROID)
-    set(OpenCV_INSTALL_PATH "${OpenCV_CONFIG_PATH}/../../..")
-  else()
-    set(OpenCV_INSTALL_PATH "${OpenCV_CONFIG_PATH}/../..")
-  endif()
+  set(OpenCV_INSTALL_PATH "${OpenCV_CONFIG_PATH}/../../..")
   # Get the absolute path with no ../.. relative marks, to eliminate implicit linker warnings
   if(${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION} VERSION_LESS 2.8)
     get_filename_component(OpenCV_INSTALL_PATH "${OpenCV_INSTALL_PATH}" ABSOLUTE)
-- 
2.7.0


From d8274c4752923dbce080775031a3a5894544a388 Mon Sep 17 00:00:00 2001
From: Niels Ole Salscheider <niels_ole@salscheider-online.de>
Date: Sat, 9 Jan 2016 15:17:02 +0100
Subject: [PATCH 2/3] Install java and 3rd party libraries to lib/OpenCV

"share" is only for architecture-independent files.

This change is important for multi-arch distributions like Exherbo that
allow to install a package for several architectures in parallel.
Here, architecture-independent files are installed to /usr/share while
all architecture-dependent files are installed to
/usr/${host}/{bin,include,lib}.
---
 CMakeLists.txt | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 7e367ad..3285f55 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -382,9 +382,9 @@ else()
     set(OPENCV_OTHER_INSTALL_PATH   etc)
   else()
     set(OPENCV_LIB_INSTALL_PATH     lib${LIB_SUFFIX})
-    set(OPENCV_3P_LIB_INSTALL_PATH  share/OpenCV/3rdparty/${OPENCV_LIB_INSTALL_PATH})
+    set(OPENCV_3P_LIB_INSTALL_PATH  lib${LIB_SUFFIX}/OpenCV/3rdparty/${OPENCV_LIB_INSTALL_PATH})
     set(OPENCV_SAMPLES_SRC_INSTALL_PATH    share/OpenCV/samples)
-    set(OPENCV_JAR_INSTALL_PATH share/OpenCV/java)
+    set(OPENCV_JAR_INSTALL_PATH lib${LIB_SUFFIX}/OpenCV/java)
     set(OPENCV_OTHER_INSTALL_PATH   share/OpenCV)
   endif()
   set(OPENCV_INCLUDE_INSTALL_PATH "include")
-- 
2.7.0


From de5f8815fe21852e7e151c40c00f64cca49cb575 Mon Sep 17 00:00:00 2001
From: Niels Ole Salscheider <niels_ole@salscheider-online.de>
Date: Sat, 9 Jan 2016 14:59:16 +0100
Subject: [PATCH 3/3] Allow to override the share install dir

This is needed for multiarch layouts where the prefix is /usr/${host}
but where arch-independet files are installed to /usr/share.
---
 CMakeLists.txt | 12 ++++++++----
 1 file changed, 8 insertions(+), 4 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 3285f55..4dff6f7 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -308,10 +308,14 @@ if (ANDROID)
   endif()
 endif()
 
+if(NOT(ANDROID OR WIN32))
+  set(OPENCV_SHARE_INSTALL_PATH share CACHE STRING "The share install directory")
+endif()
+
 if(ANDROID OR WIN32)
   set(OPENCV_DOC_INSTALL_PATH doc)
 else()
-  set(OPENCV_DOC_INSTALL_PATH share/OpenCV/doc)
+  set(OPENCV_DOC_INSTALL_PATH "${OPENCV_SHARE_INSTALL_PATH}/OpenCV/doc")
 endif()
 
 if(WIN32 AND CMAKE_HOST_SYSTEM_NAME MATCHES Windows)
@@ -353,7 +357,7 @@ if(OPENCV_TEST_DATA_PATH AND NOT OPENCV_TEST_DATA_INSTALL_PATH)
   elseif(WIN32)
     set(OPENCV_TEST_DATA_INSTALL_PATH "testdata")
   else()
-    set(OPENCV_TEST_DATA_INSTALL_PATH "share/OpenCV/testdata")
+    set(OPENCV_TEST_DATA_INSTALL_PATH "${OPENCV_SHARE_INSTALL_PATH}/OpenCV/testdata")
   endif()
 endif()
 
@@ -383,9 +387,9 @@ else()
   else()
     set(OPENCV_LIB_INSTALL_PATH     lib${LIB_SUFFIX})
     set(OPENCV_3P_LIB_INSTALL_PATH  lib${LIB_SUFFIX}/OpenCV/3rdparty/${OPENCV_LIB_INSTALL_PATH})
-    set(OPENCV_SAMPLES_SRC_INSTALL_PATH    share/OpenCV/samples)
+    set(OPENCV_SAMPLES_SRC_INSTALL_PATH    "${OPENCV_SHARE_INSTALL_PATH}/OpenCV/samples")
     set(OPENCV_JAR_INSTALL_PATH lib${LIB_SUFFIX}/OpenCV/java)
-    set(OPENCV_OTHER_INSTALL_PATH   share/OpenCV)
+    set(OPENCV_OTHER_INSTALL_PATH   "${OPENCV_SHARE_INSTALL_PATH}/OpenCV")
   endif()
   set(OPENCV_INCLUDE_INSTALL_PATH "include")
 
-- 
2.7.0

