# Copyright 2010-2011 Pierre Lejeune <superheron@gmail.com>
# Copyright 2013 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN="LibRaw"

SUMMARY="RAW decoding/processing library"
DESCRIPTION="
LibRaw is a library for reading RAW files obtained from digital photo cameras (CRW/CR2, NEF, RAF,
DNG, and others).
"
HOMEPAGE="https://www.${PN}.org"
DOWNLOADS="${HOMEPAGE}/data/${MY_PN}-${PV}.tar.gz"

LICENCES="
    || ( LGPL-2.1 CDDL-1.0 )
"
SLOT="0"
MYOPTIONS="
    examples
    jpeg2000
    lcms
    openmp
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# TODO: Adobe DNG SDK, see README.DNGSDK.txt
# TODO: rawspeed, needs to download extra files, see README.RawSpeed.txt
#    build+run:
#        rawspeed? ( dev-libs/libxml2:2.0 )
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        jpeg2000? ( media-libs/jasper )
        lcms? ( media-libs/lcms2 )
        openmp? ( sys-libs/libgomp:= )
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=8] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

WORK=${WORKBASE}/${MY_PN}-${PV}

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    examples
    'jpeg2000 jasper'
    lcms
    openmp
)

