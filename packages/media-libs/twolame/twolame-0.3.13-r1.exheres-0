# Copyright 2008 Daniel Mierswa <impulze@impulze.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.13 1.11 1.10 ] ] sourceforge [ suffix=tar.gz ]

SUMMARY="An optimised MPEG Audio Layer 2 (MP2) encoder"
DESCRIPTION="An optimised MPEG Audio Layer 2 (MP2) encoder based on tooLAME."
HOMEPAGE="http://www.${PN}.org"

REMOTE_IDS="freecode:TwoLAME"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/doc/index.html"

LICENCES="LGPL-2.1"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    frontend [[ description = [ Install twolame MP2 encoder frontend (requires libsndfile) ] ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        frontend? ( media-libs/libsndfile[>=1] )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-use-_LDADD-instead-of-AM_LDFLAGS-to-fix-dependency-d.patch
    "${FILES}"/0002-make-sndfile-dependency-and-therefore-the-frontend-o.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-static )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'frontend sndfile' )

src_install() {
    default

    # without the frontend, the manpage isn't installed
    if option !frontend ; then
        edo rm -r "${IMAGE}/usr/share/man"
    fi
}

src_test() {
    option !frontend && return

    default
}

