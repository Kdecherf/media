# Copyright 2009 Anders Ossowicki <arkanoid@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'id3lib-3.8.3-r7.ebuild' from Gentoo, which is:
#   Copyright 1999-2008 Gentoo Foundation

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 1.12 1.11 1.10 ] ]

SUMMARY="Read, write and manipulate ID3v1 and ID3v2 tags"
DESCRIPTION="id3lib is an open-source, cross-platform software development
library for reading, writing, and manipulating ID3v1 and ID3v2 tags. It is
an on-going project whose primary goals are full compliance with the ID3v2
standard, portability across several platforms, and providing a powerful and
feature-rich API with a highly stable and efficient implementation."
HOMEPAGE="http://${PN}.sourceforge.net"
DOWNLOADS="mirror://sourceforge/${PN}/${PNV}.tar.gz"

LICENCES="LGPL-2 LGPL-2.1 LGPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc"

DEPENDENCIES="
    build+run:
        sys-libs/zlib
    build:
        doc? ( app-doc/doxygen )"

UPSTREAM_DOCUMENTATION="http://id3lib.sourceforge.net/api/index.html"
REMOTE_IDS="sourceforge:id3lib freecode:id3lib"
BUGS_TO="arkanoid@exherbo.org"

AT_M4DIR=( "${WORK}/m4" )
DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}/${PNV}-zlib.patch"
    "${FILES}/${PNV}-autoconf259.patch"
    "${FILES}/${PNV}-gcc-4.3.patch"
    "${FILES}/${PNV}-test_io.patch"
    "${FILES}/${PNV}-security.patch"
    "${FILES}/${PNV}-fix-examples.patch"
)

src_compile() {
    default

    if option doc; then
        cd doc/
        doxygen Doxyfile || die "doxygen failed"
    fi
}

src_install() {
    default

    if option doc; then
        insinto /usr/share/doc/${PNVR}/html
        doins -r doc/*.html
    fi
}

