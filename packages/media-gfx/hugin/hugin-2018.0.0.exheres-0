# Copyright 2010 Xavier Barrachina <xabarci@doctor.upv.es>
# Copyright 2013 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge
require cmake [ api=2 ]
require freedesktop-desktop freedesktop-mime gtk-icon-cache

SUMMARY="Panoramic imaging toolchain based on Panorama Tools"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}releases/${PV}/en.shtml"

LICENCES="
    BSD-3   [[ note = [ src/celeste/svm.{cpp,h} ] ]]
    FDL-1.2 [[ note = [ documentation ] ]]
    GPL-2
    MIT     [[ note = [ internal copies of vigra and zthread ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    lapack [[ description = [ Use LAPACK based solver in a included library instead of the LU-based ] ]]
    openmp
    python
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# TODO: unbundle some libs:
# - levmar: unwritten, http://users.ics.forth.gr/~lourakis/levmar/
# - zthread: unwritten, http://zthread.sourceforge.net/
#            The last release is from 2005 and fails to build with recent GCCs,
#            hugin upstream made a few fixes.
#
DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        dev-libs/boost[>=1.47.0]
        dev-db/sqlite:3
        dev-libs/vigra[>=1.9.0][openexr(-)]
        graphics/exiv2[>=0.19]
        media-gfx/enblend-enfuse[>=3.2]
        media-libs/ExifTool[>=9.09]
        media-libs/glew
        media-libs/ilmbase
        media-libs/lcms2
        media-libs/libpano13[>=2.9.19]
        media-libs/libpng:=[>=1.4.0]
        media-libs/openexr
        media-libs/tiff[>=4.0]
        sci-libs/fftw[>=3.0.0]
        sci-libs/flann
        x11-dri/glu
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXi
        x11-libs/libXmu
        x11-libs/wxGTK:3.0[>=3.0]
        lapack? ( sci-libs/lapack )
        openmp? ( sys-libs/libgomp:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        python? (
            dev-lang/python:=
            dev-lang/swig[>=2.0.4]
        )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-2018.0.0-FindEXIV2.patch
    "${FILES}"/${PN}-2018.0.0-exiv2-0.27.patch
)

# TODO: Fix share install locations for cross
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_EXE_LINKER_FLAGS:STRING=-lpthread
    -DDISABLE_DPKG:BOOL=TRUE
    -DHUGIN_SHARED:BOOL=TRUE
    -DOPENEXR_INCLUDE_DIR:PATH=/usr/$(exhost --target)/include/OpenEXR
    -DUSE_GDKBACKEND_X11:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTION_BUILDS=( 'python HSI' )
CMAKE_SRC_CONFIGURE_OPTION_ENABLES=( LAPACK )
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=( 'openmp OpenMP' )

src_prepare() {
    cmake_src_prepare

    # use CMakeModules provided by CMake itself
    edo rm "${CMAKE_SOURCE}"/CMakeModules/Find{GLUT,GLEW,JPEG,LAPACK,PackageHandleStandardArgs,PkgConfig,PNG,TIFF,ZLIB}.cmake
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

