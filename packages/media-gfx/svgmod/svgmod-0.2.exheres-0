# Copyright 2009 Sterling X. Winter <replica@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require python [ python_dep=2.6 has_lib=false has_bin=true ]

SUMMARY="Command-line tool that smartly manipulates SVG image proportions"
DESCRIPTION="
SVGmod allows you to widen the aspect ratio of an SVG image without distorting
the proportions and relative positioning of select image elements, and without
cropping or otherwise discarding visual information. It can also scale SVG
images up or down, export to PNG format, and set a background color. SVGmod is
particularly useful for scripts that automate the adaptation of desktop
wallpaper, display manager backgrounds, boot splashes, and GRUB menu splashes
to multiple display geometries or color schemes.
"
HOMEPAGE="http://gitorious.org/${PN}/"
DOWNLOADS="http://gitorious.org/${PN}/releases/blobs/raw/master/${PNV}.tar.bz2"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"

DEPENDENCIES="
    build+run:
        dev-python/lxml
    run:
        gnome-desktop/librsvg
"

BUGS_TO="replica@exherbo.org"
UPSTREAM_CHANGELOG="${HOMEPAGE}${PN}/commits/master/"
UPSTREAM_DOCUMENTATION="${HOMEPAGE}pages/Home/ [[ lang = en ]]"

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( README.mkd )

src_install() {
    default
    dobin svgmod
    doman svgmod.1
}

