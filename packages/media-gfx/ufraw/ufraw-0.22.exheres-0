# Copyright 2010 Stephen P. Becker <spbecker@exherbo.org>
# Copyright 2013 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the therms of the GNU General Public License v2

require sourceforge [ suffix=tar.gz ] \
     freedesktop-desktop \
     freedesktop-mime

SUMMARY="UFRaw is a utility to read and manipulate raw images from digital cameras"
DESCRIPTION="
The UFRaw (Unidentified Flying Raw) is a tool for reading and manipulating raw images from digital
cameras. It supports most of the existing raw formats (any format supported by DCRaw). UFRaw can be
used on its own or as a Gimp plug-in.
"
HOMEPAGE+=" http://ufraw.sourceforge.net/index.html"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gimp [[
        description = [ Build the GIMP plugin ]
        requires = [ gui ]
    ]]
    gui [[ description = [ Build the GTK2-based graphical user interface ] ]]
    openmp [[ description = [ Support for Open Multi-Processing ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

# cfitsio: automagic
DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-arch/bzip2
        dev-libs/atk
        dev-libs/glib:2[>=2.12]
        graphics/exiv2[>=0.20]
        media-libs/jasper
        media-libs/lcms2
        media-libs/lensfun[>=0.2.5]
        media-libs/libpng:=[>=1.2]
        media-libs/tiff
        sys-libs/zlib
        gimp? ( media-gfx/gimp[>=2.2.0] )
        gui? (
            media-libs/gtkimageview[>=1.6]
            x11-libs/gtk+:2[>=2.12]
        )
        openmp? ( sys-libs/libgomp:= )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-contrast
    --enable-dst-correction
    --enable-extras
    --enable-mime
    --enable-openmp
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    openmp
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    gimp
    'gui gtk'
)

DEFAULT_SRC_COMPILE_PARAMS=( AR=${AR} )

src_install() {
    default

    insinto /usr/share/mime/packages
    doins ${PN}-mime.xml

    ! option gui && edo rm -rf "${IMAGE}"/usr/share/applications
}

pkg_postinst() {
    option gui && freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
}

pkg_postrm() {
    option gui && freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
}

